var restful = require('node-restful');
module.exports = function(app, route) {

  // Setup the controller for REST;
  //Resource(app, '', route, app.models.movie).rest();
var rest = restful.model(
	'movie',
	app.models.movie
).methods(['get', 'put', 'post', 'delete']);

rest.register(app, route);

  // Return middleware.
  return function(req, res, next) {
    next();
  };
};